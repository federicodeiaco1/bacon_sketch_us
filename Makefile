
.PHONY: clean default

default:
	gcc ./src/* -o bacon -lpcap -lm -lpthread

clean:
	rm bacon
