#include "../include/bacon.h"

void calculate_thresholds(const unsigned int *E_dsts, unsigned int *threshold, pthread_mutex_t *lock){
    //Arrays used for store the Estimated E_dst and Dev E_dst values in order to calculate
    //the exponential weighted moving average (ewma).
    //Estimated E_dst; calculated as Est_E_dst_t_i = (1-alpha)*Est_E_dst_t_i-1 + alpha*E_dst_t_i
    //Dev E_dst; calculated as Dev_E_dst_t_i = (1-beta)*Dev_E_dst_t_i-1 + beta*|E_dst_t_i - Est_E_dst_t_i|
    static float Est_E_dst[w_bs];
    static float Dev_E_dst[w_bs];
    
    unsigned int th;

    //Used for calculating SMA
    float count = 0, thresholds_sums = 0;

    for(int i = 0; i < w_bs; i++){
        if(E_dsts[i] != 0){//Check if it is a never reached destination
            Est_E_dst[i] = (1 - alpha)*Est_E_dst[i] + alpha*E_dsts[i];
            Dev_E_dst[i] = (1 - beta)*Dev_E_dst[i] + beta*((float)fabs(E_dsts[i] - Est_E_dst[i]));
            thresholds_sums += (unsigned int)round(Est_E_dst[i] + sm*Dev_E_dst[i]); //Cumulate the threshold in order to calculate the SMA
            count++;

            if(*threshold != UINT32_MAX)
                printf("index: %d E_dst retreived:%u Th:%u\n", i, E_dsts[i], *threshold); 
        }
    }

    th = round(thresholds_sums / count);
    if(th > 0){
        printf("***NEW THRESHOLD:%d***\n", th);
        pthread_mutex_lock(lock);
        *threshold = th;
        pthread_mutex_unlock(lock);
    }
}


//p --> pointer to a data structure to reset
//size --> data structure size
void reset_map(const void *p, const unsigned int size, const size_t element_size){     
    
    #pragma omp parallel
    {
        #pragma omp for
        for (int i = 0; i < size; i++){
            if(element_size == sizeof(unsigned int))
                ((unsigned int *)p)[i] = 0;
            else if (element_size == sizeof(bool))
                ((bool *)p)[i] = false;
        }
    }
}

void *bacon(void *arg) {
    //Reading args struct
    Thread_args *args = (Thread_args *)arg;

    unsigned char sel;
    bool *bacon_sketch_sel;
    unsigned int *E_dsts_sel;
    unsigned int *countmin_sketch_sel;
    bool *is_updating_sel;

    unsigned int tw_counter = 1;

    //Flags for automatically stop the program, if for two sequently time windows there are 0 packets end the program
    bool zero_packets_1 = false, zero_packets_2 = false;
    unsigned int pkt_counter = 0, last_pkt_counter = 1;

    while(!zero_packets_1 || !zero_packets_2){
        usleep(T_int * 1000000);

        pkt_counter = *(args->pkt_count);
        //Reading number of packet processed and check if end the program
        if(pkt_counter == last_pkt_counter && !zero_packets_1 && !zero_packets_2){
            zero_packets_1 = true;
        } else if (pkt_counter != last_pkt_counter && zero_packets_1 && !zero_packets_2){
            zero_packets_1 = false;
        } else if (pkt_counter == last_pkt_counter && zero_packets_1 && !zero_packets_2){
            zero_packets_2 = true;
            printf("***Second time window with 0 packets, terminating the program***\n");
            break;
        }
        last_pkt_counter = pkt_counter;

        sel = *(args->bs_sel);
        //Selecting bacon sketch and E_dsts to use
        if(sel){
            bacon_sketch_sel = args->bacon_sketch_1;
            E_dsts_sel = args->E_dsts_1;
            countmin_sketch_sel = args->cm_sketch_1;
            is_updating_sel = args->is_updating_1;

        } else{
            bacon_sketch_sel = args->bacon_sketch_0;
            E_dsts_sel = args->E_dsts_0;
            countmin_sketch_sel = args->cm_sketch_0;
            is_updating_sel = args->is_updating_0;
        }

        sel = sel ^ 0x01;
        //Updating selector value
        pthread_mutex_lock(&(args->lock));
        *(args->bs_sel) = sel;
        *is_updating_sel = true;
        pthread_mutex_unlock(&(args->lock));


        printf("Start calculating\n");
        //Calculate thresholds for each E_dst
        calculate_thresholds(E_dsts_sel, args->threshold, &(args->lock));
        printf("Done thresholds\n");

        //Reset the bacon sketch structure and E_dsts calculated values
        reset_map(bacon_sketch_sel, m_bs*w_bs*d_bs, sizeof(bool));
        reset_map(E_dsts_sel, w_bs, sizeof(unsigned int));
        reset_map(countmin_sketch_sel, w_bs * d_bs, sizeof(unsigned int));

        pthread_mutex_lock(&(args->lock));
        *is_updating_sel = false;
        pthread_mutex_unlock(&(args->lock));

        printf("Ended ops for the time window:%u\n", tw_counter);
        tw_counter += 1;
    }

    //terminate_program(); Should this be here or in main()?
    return NULL;
}