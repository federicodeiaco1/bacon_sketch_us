#include "../include/main.h"

void init_args(Thread_args *args){
    //Init bacon sketches
    args->bacon_sketch_0 = (bool *)calloc(m_bs*w_bs*d_bs, sizeof(bool));
    args->bacon_sketch_1 = (bool *)calloc(m_bs*w_bs*d_bs, sizeof(bool));

    //Init E_dsts arrays
    args->E_dsts_0 = (unsigned int *)calloc(w_bs, sizeof(unsigned int));
    args->E_dsts_1 = (unsigned int *)calloc(w_bs, sizeof(unsigned int));

    //Init auxiliary cm sketches
    args->cm_sketch_0 = (unsigned int *)calloc(w_bs * d_bs, sizeof(unsigned int));
    args->cm_sketch_1 = (unsigned int *)calloc(w_bs * d_bs, sizeof(unsigned int));

    //Init selector
    args->bs_sel = (unsigned char *)malloc(sizeof(unsigned char));

    //Init global threshold
    args->threshold = (unsigned int *)malloc(sizeof(unsigned int));
    *(args->threshold) = UINT32_MAX;

    //Init updating flags
    args->is_updating_0 = (bool *)malloc(sizeof(bool));
    args->is_updating_1 = (bool *)malloc(sizeof(bool));

    //Init pkts counter
    args->pkt_count = (unsigned int *)malloc(sizeof(unsigned int));

    //Init interface name to null, this will get the value from argv
    args->if_name = NULL;

    //Init lock
    pthread_mutex_init(&(args->lock), NULL);

}

int main(int argc, char *argv[]){

    signal(SIGINT, signal_callback_handler);

    if(argc != 2){
        printf("Error: not enough arguments\nUsage: ./bacon <ifname>\n");
        return 1;
    }
    
    Thread_args *args = (Thread_args *)malloc(sizeof(Thread_args)); 
    if(args == NULL){
        printf("Memory allocation args failed\nABORTING\n");
        return 1;
    }

    init_args(args);
    args->if_name = argv[1];
    printf("Allocated all needed data\nStarting threads...\n");

    //Creating threads
    //t_pcap --> thread that reads the packets and process them
    //t_bacon --> thread that updates the threshold and resets bacon sketch
    pthread_t t_pcap, t_bacon;
    pthread_create(&t_pcap, NULL, read_packets, (void *)args);
    pthread_create(&t_bacon, NULL, bacon, (void *)args);

    //Wait for bacon thread to finish
    pthread_join(t_bacon, NULL);
    //Cancel the pcap thread since it's always in pcap_loop() function
    pthread_cancel(t_pcap);

    printf("Packets read: %d\n", *(args->pkt_count));    
    free(args->bacon_sketch_0);
    free(args->bacon_sketch_1);
    free(args->bs_sel);
    free(args->cm_sketch_0);
    free(args->cm_sketch_1);
    free(args->E_dsts_0);
    free(args->E_dsts_1);
    free(args->is_updating_0);
    free(args->is_updating_1);
    free(args->pkt_count);
    pthread_mutex_destroy(&(args->lock));
    free(args);

    return 0;
}