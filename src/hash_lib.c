#include "../include/hash_lib.h"

//h_bm function
unsigned int fnv1a_hash(unsigned char *data, int len) {
    unsigned int hash = 2166136261U; // FNV offset basis

    for (int i = 0; i < len; i++) {
        hash ^= data[i];
        hash *= 16777619U; // FNV prime
    }

    return hash;
}

//h_cm first function
unsigned int djb2_hash(unsigned char *data, int len) {
    unsigned int hash = 5381;

    for (int i = 0; i < len; i++) {
        hash = ((hash << 5) + hash) + data[i];
    }

    return hash;
}

//h_cm second function
unsigned int bernstein_hash(unsigned char *data, int len) {
    unsigned int hash = 5381;

    for (int i = 0; i < len; i++) {
        hash = 33 * hash ^ data[i];
    }

    return hash;
}

//h_cm third function
unsigned int oat_hash(unsigned char *data, int len) {
    unsigned int hash = 0;

    for (int i = 0; i < len; i++) {
        hash += data[i];
        hash += (hash << 10);
        hash ^= (hash >> 6);
    }

    hash += (hash << 3);
    hash ^= (hash >> 11);
    hash += (hash << 15);

    return hash;
}