#include "../include/packet_reader.h"

void extract_keys(key_src *k_src, key_dst *k_dst, const unsigned char *buffer){
    struct ethhdr *eth = (struct ethhdr *)buffer;

    // Check that it's an IP packet
    if(ntohs(eth->h_proto) == ETH_P_IP){
        struct iphdr *iph = (struct iphdr *)(buffer + sizeof(struct ethhdr));
        for(int i = 0; i < 4; i++){
            k_src->d_addr[i] = (iph->daddr >> (8*i)) & 0xFF;
            k_src->s_addr[i] = (iph->saddr >> (8*i)) & 0xFF;
            k_dst->d_addr[i] = (iph->daddr >> (8*i)) & 0xFF; 
        }
        // 6 = TCP           
        if(iph->protocol == 6){
            struct tcphdr *tcph = (struct tcphdr *)(buffer + sizeof(struct ethhdr) + sizeof(struct iphdr));
            k_src->s_port = ntohs(tcph->source);
            k_src->d_port = ntohs(tcph->dest);
            k_dst->d_port = ntohs(tcph->dest); //VS Code gives error but actually compiles
        }
        // 17 = UDP 
        else if(iph->protocol == 17){
            struct udphdr *udph = (struct udphdr *)(buffer + sizeof(struct ethhdr) + sizeof(struct iphdr));
            k_src->s_port = ntohs(udph->source);
            k_src->d_port = ntohs(udph->dest);
            k_dst->d_port = ntohs(udph->dest);
            
        }

    }else{
        printf("Not an IP packet, protocol value: %d\n", ntohs(eth->h_proto)); 
    }
}

void update_query(key_src *k_src, key_dst *k_dst, Thread_args *args, unsigned int *E_dst){
    //Array of pointers to function
    unsigned int (*hash_f[])(unsigned char *, int)={djb2_hash, bernstein_hash, oat_hash};
    unsigned short bucket = fnv1a_hash((unsigned char *)k_src, sizeof(key_src)) % m_bs;
    unsigned int E_i = 0xffffffff, id = 0, index = 0;

    //For each hash function h_cm
    for(unsigned char i=0; i < d_bs; i++){
        id = (hash_f[i]((unsigned char *)k_dst, sizeof(key_dst)) % w_bs);

        index = id * m_bs + bucket + i*w_bs*m_bs;

        bool *bs_val = NULL;
        if(*(args->bs_sel))
            bs_val = &((args->bacon_sketch_1)[index]);
        else
            bs_val = &((args->bacon_sketch_0)[index]);
        
        if(bs_val == NULL){
            printf("Error reading bacon sketch value\n");
            return;
        }
        
        //If the value in the bacon sketch is 0, turn it to 1 and update the E_dst counter in the auxiliary countmin sketch
        unsigned int *cms_val = NULL;
        //Reading the value inside the auxiliary count min sketch
        id = id + i * w_bs;

        if(*(args->bs_sel))
            cms_val = &((args->cm_sketch_1)[id]);
        else
            cms_val = &((args->cm_sketch_1)[id]);
        
        if(cms_val == NULL){
            printf("Error reading countmin sketch value\n");
            return; 
        }
        
        if(*bs_val == false){
            pthread_mutex_lock(&(args->lock));
            *cms_val = *cms_val + 1;
            *bs_val = true;
            pthread_mutex_unlock(&(args->lock));
        }

        if(*cms_val < E_i)
            E_i = *cms_val;
        if(*E_dst == 0)
            *E_dst = E_i;
        else if(E_i < *E_dst)
            *E_dst = E_i;
    }
}

//This function is triggered for each incoming packet
void process_packet(Thread_args *args, const struct pcap_pkthdr *header, const unsigned char *buffer){
    unsigned int E_dst = 0;
    //Updating pkt counter
    pthread_mutex_lock(&(args->lock));
    *(args->pkt_count) = *(args->pkt_count) + 1;
    pthread_mutex_unlock(&(args->lock));

    bool flag;
    if(*(args->bs_sel))
        flag = *(args->is_updating_1);
    else
        flag = *(args->is_updating_0);

    key_src k_src;
    key_dst k_dst;

    extract_keys(&k_src, &k_dst, buffer);
    update_query(&k_src, &k_dst, args, &E_dst);

    unsigned int index = djb2_hash((unsigned char *)&k_dst, sizeof(key_dst)) % w_bs;
    if(*(args->bs_sel))
        (args->E_dsts_1)[index] = E_dst;
    else
        (args->E_dsts_0)[index] = E_dst;
    
    unsigned int th = *(args->threshold);

    if(th > 0){
        //DEBUG PRINT TODO
        if(E_dst > th){
            //mark packet as malicious, what am I supposed to do?
        }
    }    
}

void *read_packets(void *arg) {
    Thread_args *args = (Thread_args *)arg;

    pcap_t *handle;
    char errbuf[100];

    printf("Opening %s interface...\n", args->if_name);
    handle = pcap_open_live(args->if_name, 65536, 1, 100, errbuf);
    if(handle == NULL){
        printf("Couldn't open device %s: %s", args->if_name, errbuf);
        exit(1);
    }
    printf("DONE!\n");

    pcap_loop(handle, -1, (pcap_handler)process_packet, (unsigned char *)args);

    return NULL;
}