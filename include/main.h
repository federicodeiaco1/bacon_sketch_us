#ifndef MAIN_H
#define MAIN_H

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <signal.h>
#include "common.h"
#include "bacon.h"
#include "packet_reader.h"

void terminate_program(){
    printf("terminating program\n");
}

void signal_callback_handler(int signum){
    terminate_program();
    exit(signum);
}


#endif