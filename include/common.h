#ifndef COMMON_H
#define COMMON_H

#include <pthread.h>
#include <stdbool.h>

#define m_bs 1024 //bitmap register size
#define w_bs 1024 //bacon sketch columns
#define d_bs 3 //bacon sketch rows, corresponds to the number of hash function used

struct Thread_args{
    bool *bacon_sketch_0, *bacon_sketch_1, *is_updating_0, *is_updating_1;
    unsigned int *E_dsts_0, *E_dsts_1, *cm_sketch_0, *cm_sketch_1, *threshold, *pkt_count;
    unsigned char *bs_sel;
    char *if_name;
    pthread_mutex_t lock;
};
typedef struct Thread_args Thread_args;

#endif