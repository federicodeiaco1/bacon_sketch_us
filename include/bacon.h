#ifndef BACON_H
#define BACON_H

#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <math.h>
#include <string.h>
#include <omp.h>
#include "hash_lib.h"
#include "common.h"

#define alpha 0.875
#define beta 0.125
#define sm 4 //Safety margin
#define T_int 10

void *bacon(void *arg);


#endif