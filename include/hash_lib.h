#ifndef HASHLIB_H
#define HASHLIB_H

//h_bm function
unsigned int fnv1a_hash(unsigned char *data, int len);

//h_cm first function
unsigned int djb2_hash(unsigned char *data, int len);

//h_cm second function
unsigned int bernstein_hash(unsigned char *data, int len);

//h_cm third function
unsigned int oat_hash(unsigned char *data, int len);

#endif
