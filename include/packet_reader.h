#ifndef PACKET_READER_H
#define PACKET_READER_H

#include <stdlib.h>
#include <pcap/pcap.h>
#include <net/ethernet.h>
#include <netinet/in.h>
#include <netinet/udp.h> //Provides declarations for udp header
#include <netinet/tcp.h> //Provides declarations for tcp header
#include <netinet/ip.h>	//Provides declarations for ip header
#include "hash_lib.h"
#include "common.h"

#define ETH_P_IP 0x0800

//Key used for the bitmap register's related hash function
//Must include the source IP of the flow together
//with any subset of {source port, destination IP and port, protocol}
struct key_src{
    unsigned char s_addr[4];
    unsigned short int s_port;
    unsigned char d_addr[4];
    unsigned short int d_port;
};
typedef struct key_src key_src;

//Key used for the BACON Sketch's related hash functions (they are d)
//Can be either the destination IP or the {destination IP, destination port} pair.
struct key_dst{
    unsigned char d_addr[4];
    unsigned short int d_port;
};
typedef struct key_dst key_dst;

void *read_packets(void *arg);

#endif