This is the user space implementation of eBPF BACON sketch:
    https://gitlab.com/federicodeiaco1/bacon_sketch

The packet capture is provided by libpcap C library:
    https://www.tcpdump.org/

The main logic is that there are 2 threads that share Thread_args (bacon_sketch data etc...)
    - The first thread reads packets with libpcap functions and for each packet performs the "eBPF part"
    - The second thread wakes up each T_int seconds and calculate thresholds